all:
	./gradlew clean
	./gradlew distZip

repl:
	./gradlew compileJava && rlwrap java -cp build/classes/java/main/ serene.lambda.Main

nrepl:
	./gradlew compileJava && rlwrap java -cp build/classes/java/main/ serene.lambda.Main nrepl

run:
	./gradlew compileJava && java -cp build/classes/java/main/ serene.lambda.Main ${PWD}/test.srns

docs:
	npx docco src/**/*.java
