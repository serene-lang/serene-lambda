# Serene (lambda) version
**Serene lambda** is a super minimal lisp that is implemented in Java as a proof of concept and as an experience.
For more info checkout my [blog](https://lxsameer.com/).

## Requirements
* JDK >= 1.8
* rlwrap
* gradle

## Repl
in order to run the REPL, run `make repl`

## Specification
TBD
