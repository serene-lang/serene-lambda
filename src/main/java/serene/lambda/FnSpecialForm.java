/**
 * Serene (lambda) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */
package serene.lambda;

import java.util.function.Function;

public class FnSpecialForm extends SpecialForm {
  public FnSpecialForm(ListNode<Node> paramsAndBody) {
    super(paramsAndBody);
  }

  @Override
  public Object eval(final IScope parentScope) {
    final ListNode<Node> formalParams = (ListNode<Node>) this.node.rest().first();
    final ListNode<Node> body = this.node.rest().rest();

    return new Function<Object, Object>() {
      @Override
      public Object apply(Object arguments) {
        Object[] args = (Object[]) arguments;

        Scope scope = new Scope(parentScope);
        if (args.length != formalParams.length) {
          throw new RuntimeException(
            String.format("Wrong number of arguments. Expected: %s, Got: %s. ARGS: %s",
                          formalParams.length,
                          args.length,
                          args.toString()));
        }
        // Map parameter values to formal parameter names
        int i = 0;
        for (Node param : formalParams) {
          SymbolNode paramSymbol = (SymbolNode) param;
          scope.insertSymbol(paramSymbol.name, args[i]);
          i++;
        }

        // Evaluate body
        Object output = null;
        for (Node node : body) {
          output = node.eval(scope);
        }

        return output;
      }

      @Override
      public String toString() {
        return "Function@" + System.identityHashCode(this);
      }
    };
  }
}
