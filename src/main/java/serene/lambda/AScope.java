
/**
 * Serene (lambda) - A PoC lisp to collect data on Serenes concepts
 * Copyright (C) 2019-2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

package serene.lambda;

import java.util.HashMap;
import java.util.Map;

public abstract class AScope implements IScope {
  private Map<String, Object> symbolsMap = new HashMap<String, Object>();

  protected IScope parentScope;

  public AScope() {
    this(null);
  }

  public AScope(IScope parent) {
    this.parentScope = parent;
  }

  public IScope parent() {
    return this.parentScope;
  }

  public Map<String, Object> symbols() {
    return this.symbolsMap;
  }

  public Object lookupSymbol(String symbolName) {
    Object value = this.getLocalSymbol(symbolName);

    if (!(value instanceof Null)) {
      return value;
    }
    else if (this.parent() != null) {
      return this.parent().lookupSymbol(symbolName);
    }
    else {
      throw new RuntimeException(
        String.format("Symbol '%s' is not defined in this scope.", symbolName));
    }
  }

  public void insertSymbol(String symbolName, Object symbolValue) {
    this.symbols().put(symbolName, symbolValue);
  }

  protected Object getLocalSymbol(String symbolName) {
    if (this.symbols().containsKey(symbolName)) {
      return this.symbols().get(symbolName);
    }

    return new Null();
  }

  private class Null {}
}
